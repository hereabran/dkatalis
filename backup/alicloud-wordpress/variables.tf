variable "ac_region" {
  type        = string
  description = "Alicloud Region"
}

variable "provision_name" {
  type = string
  description = "Provision Infrastructure name"
}

variable "provision_prefix" {
  type = string
  description = "Provision Infrastructure prefix"
}