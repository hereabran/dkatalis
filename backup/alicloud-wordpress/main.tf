provider "alicloud" {}

resource "alicloud_vpc" "dk_vpc" {
  vpc_name   = local.name
  cidr_block = "172.16.0.0/16"
}

resource "alicloud_security_group" "dk_security_group" {
  name   = local.name
  vpc_id = alicloud_vpc.dk_vpc.id
}

resource "alicloud_security_group_rule" "dk_allow_http" {
  type              = "ingress"
  ip_protocol       = "tcp"
  nic_type          = "intranet"
  policy            = "accept"
  port_range        = "80/80"
  priority          = 1
  security_group_id = alicloud_security_group.dk_security_group.id
  cidr_ip           = "0.0.0.0/0"
}

resource "alicloud_security_group_rule" "dk_allow_int_ssh" {
  type              = "ingress"
  ip_protocol       = "tcp"
  nic_type          = "intranet"
  policy            = "accept"
  port_range        = "22/22"
  priority          = 1
  security_group_id = alicloud_security_group.dk_security_group.id
  cidr_ip           = "172.16.0.0/24"
}

resource "alicloud_vswitch" "dk_vswitch" {
  vpc_id       = alicloud_vpc.dk_vpc.id
  cidr_block   = "172.16.0.0/24"
  zone_id      = data.alicloud_zones.dk_zones.zones[0].id
  vswitch_name = local.name
}

# ECS Instance & SLB Configuration
resource "alicloud_slb_load_balancer" "dk_slb" {
  load_balancer_name = local.name
  address_type       = "intranet"
  load_balancer_spec = "slb.s2.small"
  vswitch_id         = alicloud_vswitch.dk_vswitch.id
}

resource "alicloud_instance" "dk_wordpress" {
  # cn-beijing
  availability_zone = data.alicloud_zones.dk_zones.zones.0.id
  security_groups   = alicloud_security_group.dk_security_group.*.id

  # series III
  instance_type              = data.alicloud_instance_types.dk_instance_type.instance_types.0.id
  system_disk_category       = "cloud_efficiency"
  system_disk_name           = "${local.name}_system_disk_name"
  system_disk_description    = "${local.name}_system_disk_description"
  image_id                   = data.alicloud_images.ubuntu.images.0.id
  instance_name              = local.name
  vswitch_id                 = alicloud_vswitch.dk_vswitch.id
  internet_max_bandwidth_out = 10
}

resource "alicloud_image" "dk_image" {
  instance_id  = alicloud_instance.dk_wordpress.id
  image_name   = local.name
  description  = "test-image"
  architecture = "x86_64"
  platform     = "Ubuntu"
}

# ESS Auto Scaling Configuration
resource "alicloud_ess_scaling_group" "dk_ess_scaling_group" {
  min_size           = 1
  max_size           = 2
  scaling_group_name = local.name
  vswitch_ids        = [alicloud_vswitch.dk_vswitch.id]
  removal_policies   = ["OldestInstance", "NewestInstance"]
}

resource "alicloud_ess_scaling_configuration" "dk_ess_scaling_config" {
  scaling_group_id  = alicloud_ess_scaling_group.dk_ess_scaling_group.id
  image_id          = alicloud_image.dk_image.id
  instance_type     = data.alicloud_instance_types.dk_instance_type.instance_types[0].id
  security_group_id = alicloud_security_group.dk_security_group.id
  force_delete      = true
  active            = true
}

resource "alicloud_ess_scaling_rule" "dk_ess_scaling_rule" {
  scaling_group_id = alicloud_ess_scaling_group.dk_ess_scaling_group.id
  metric_name      = "CpuUtilization"
  target_value     = 80
  adjustment_type  = "TotalCapacity"
  adjustment_value = 1
}

resource "alicloud_ess_alarm" "dk_ess_alarm" {
  name                = "tf-testAccEssAlarm_basic"
  description         = "Acc alarm test"
  alarm_actions       = [alicloud_ess_scaling_rule.dk_ess_scaling_rule.ari]
  scaling_group_id    = alicloud_ess_scaling_group.dk_ess_scaling_group.id
  metric_type         = "system"
  metric_name         = "CpuUtilization"
  period              = 60
  statistics          = "Average"
  threshold           = 60
  comparison_operator = ">="
  evaluation_count    = 2
}
