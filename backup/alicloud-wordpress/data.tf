locals {
  name   = "${var.provision_prefix}-${var.provision_name}"
  prefix = var.provision_prefix
}

data "alicloud_zones" "dk_zones" {
  available_resource_creation = "VSwitch"
  available_disk_category     = "cloud_efficiency"
}

data "alicloud_images" "dk_ubuntu" {
  name_regex  = "^ubuntu_18.*64"
  most_recent = true
  owners      = "system"
}

data "alicloud_instance_types" "dk_instance_type" {
  availability_zone = data.alicloud_zones.dk_zones.zones.0.id
  cpu_core_count    = 2
  memory_size       = 4
}

output "availibility_zone" {
  value = data.alicloud_zones.dk_zones.zones.0.id
}

output "image" {
  value = data.alicloud_images.dk_ubuntu.images.0.id
}

output "instance_type" {
  value = data.alicloud_instance_types.dk_instance_type.instance_types.0.id
}
