version: \"3.9\"
services:
  db:
    image: mysql:5.7
    volumes:
      - db_data:/var/lib/mysql
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: ${db_root_pass}
      MYSQL_DATABASE: ${db_name}
      MYSQL_USER: ${db_user}
      MYSQL_PASSWORD: ${db_pass}
  wordpress:
    depends_on:
      - db
    image: wordpress:latest
    volumes:
      - wordpress_data:/var/www/html
    restart: always
    environment:
      WORDPRESS_DB_HOST: db
      WORDPRESS_DB_USER: ${db_user}
      WORDPRESS_DB_PASSWORD: ${db_pass}
      WORDPRESS_DB_NAME: ${db_name}
  nginx:
    depends_on:
      - wordpress
    image: nginx:1.18-alpine
    restart: always
    ports:
      - \"80:80\"
    volumes:
      - ./nginx:/etc/nginx/conf.d
volumes:
  db_data: {}
  wordpress_data: {}