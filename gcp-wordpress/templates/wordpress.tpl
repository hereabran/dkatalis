server {
    listen ${external_port};

    location / {
        proxy_pass                  http://wordpress:80;
        proxy_set_header            HOST \$host;
        proxy_set_header            X-Real-Ip \$remote_addr;
        proxy_set_header            X-Forwarded-Proto \$scheme;
        proxy_set_header            X-Forwarded-For \$proxy_add_x_forwarded_for;
    }
}