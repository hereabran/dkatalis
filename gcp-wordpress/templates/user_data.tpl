#!/bin/bash

# Set Timezone to Asia/Jakarta
echo "export TZ=Asia/Jakarta" >> ~/.bashrc
source ~/.bashrc

# Install Docker
sudo apt update
sudo curl -fsSL https://get.docker.com | sh -
sudo usermod -aG docker $USER
sudo usermod -aG docker ubuntu

# Install Docker Compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# Setup Wordpress Components and Run service
sudo mkdir -p /var/opt/wp/nginx
cd /var/opt/wp

echo "${docker_compose}" >> docker-compose.yml
echo "${nginx_conf}" >> nginx/wordpress.conf

docker-compose up
