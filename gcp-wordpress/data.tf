locals {
  name   = "${var.provision_prefix}-${var.provision_name}"
  prefix = var.provision_prefix
  subnets = {
    for i, x in var.gcp_subnets :
    i => x
  }
}

data "google_compute_zones" "dk_zones" {
  project = var.gcp_project_id
  region  = var.gcp_region
}

data "google_compute_image" "dk_ubuntu" {
  family  = "ubuntu-1804-lts"
  project = "ubuntu-os-cloud"
}

resource "random_password" "dk_password" {
  length           = 16
  special          = true
  override_special = "_%@"
}

data "template_file" "dk_docker_compose" {
  template = file("${path.module}/templates/docker-compose.tpl")
  vars = {
    db_root_pass = var.wp_db_root_pass
    db_name      = var.wp_db_name
    db_user      = var.wp_db_user
    db_pass      = var.wp_db_pass
  }
}

data "template_file" "dk_nginx_conf" {
  template = file("${path.module}/templates/wordpress.tpl")
  vars = {
    external_port = var.wp_external_port
  }
}

data "template_file" "dk_user_data" {
  template = file("${path.module}/templates/user_data.tpl")
  vars = {
    docker_compose = data.template_file.dk_docker_compose.rendered
    nginx_conf     = data.template_file.dk_nginx_conf.rendered
  }
}

output "dk_zones" {
  value = data.google_compute_zones.dk_zones.names[0]
}

output "dk_wordpress_addr" {
  value = google_compute_global_address.dk_wordpress_addr.address
}