variable "provision_name" {
  type = string
  description = "Provision Infrastructure name"
}

variable "provision_prefix" {
  type = string
  description = "Provision Infrastructure prefix"
}

variable "gcp_project_id" {
  type        = string
  description = "Google Project ID"
}

variable "gcp_region" {
  type        = string
  description = "Google Cloud Platform Region"
}

variable "gcp_routing_mode" {
  type        = string
  description = "GCP VPC Routing Mode"
}

variable "gcp_subnets" {
  type        = map(string)
  description = "GCP VPC Subnetworks"
}

variable "machine_type" {
  type = string
}

variable "target_tags" {
  type = list(string)
}

variable "instance_labels" {
  type = map(string)
}

variable "disk_type" {
  type = string
}

variable "disk_size_gb" {
  type = number
}

variable "wp_db_name" {
  type = string
}

variable "wp_db_root_pass" {
  type = string
}

variable "wp_db_user" {
  type = string
}

variable "wp_db_pass" {
  type = string
}

variable "wp_external_port" {
  type = number
}