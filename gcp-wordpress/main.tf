# Providers
provider "google" {
  project = var.gcp_project_id
  region  = var.gcp_region
}

# VPC  and Networks
resource "google_compute_network" "dk_vpc" {
  project                         = var.gcp_project_id
  name                            = "${local.prefix}-net"
  auto_create_subnetworks         = false
  routing_mode                    = var.gcp_routing_mode
  delete_default_routes_on_create = true
  mtu                             = 1460
}

resource "google_compute_subnetwork" "dk_subnets" {
  name                     = local.subnets.subnet_name
  ip_cidr_range            = local.subnets.subnet_ip
  region                   = local.subnets.subnet_region
  private_ip_google_access = true

  log_config {
    metadata             = lookup(local.subnets, "subnet_flow_logs_metadata", null)
  }

  network     = google_compute_network.dk_vpc.name
  project     = var.gcp_project_id
  description = lookup(local.subnets, "description", null)
}

// resource "google_compute_firewall" "dk_allow_ssh" {
//   project = var.gcp_project_id
//   name    = "${local.prefix}-allow-ssh"
//   network = google_compute_network.dk_vpc.name

//   allow {
//     protocol = "tcp"
//     ports    = ["22"]
//   }

//   source_ranges = ["0.0.0.0/0"]
//   target_tags   = ["dk-allow-ssh"]
// }

resource "google_compute_firewall" "dk_allow_http" {
  project = var.gcp_project_id
  name    = "${local.prefix}-allow-http"
  network = google_compute_network.dk_vpc.name

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["dk-allow-http"]
}

# allow access from health check ranges
resource "google_compute_firewall" "dk_allow_hc" {
  name          = "${local.prefix}-allow-hc"
  direction     = "INGRESS"
  network       = google_compute_network.dk_vpc.id
  source_ranges = ["130.211.0.0/22", "35.191.0.0/16"]
  allow {
    protocol = "tcp"
    ports    = ["80"]
  }
  target_tags = ["allow-health-check"]
}


resource "google_compute_address" "dk_nat_addr" {
  name = "${local.prefix}-nat-addr"
}

resource "google_compute_global_address" "dk_wordpress_addr" {
  name = "${local.prefix}-wordpress-addr"
}

resource "google_compute_route" "dk_route" {
  name             = "egress-internet"
  dest_range       = "0.0.0.0/0"
  network          = google_compute_network.dk_vpc.name
  tags             = ["egress-inet"]
  priority         = 100
  next_hop_gateway = "default-internet-gateway"
}

resource "google_compute_router" "dk_router" {
  name    = "${local.prefix}-router"
  network = google_compute_network.dk_vpc.name
  region  = var.gcp_region
}

resource "google_compute_router_nat" "dk_nat" {
  name                               = "${local.prefix}-nat"
  router                             = google_compute_router.dk_router.name
  region                             = var.gcp_region
  nat_ip_allocate_option             = "MANUAL_ONLY"
  nat_ips                            = google_compute_address.dk_nat_addr.*.self_link
  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"

  subnetwork {
    name                    = google_compute_subnetwork.dk_subnets.id
    source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
  }

  log_config {
    enable = true
    filter = "ERRORS_ONLY"
  }
}

# Service account for VMs
resource "google_service_account" "dk_wordpress_sa" {
  account_id = "${local.prefix}-wordpress"
}

# Instance Template
resource "google_compute_instance_template" "dk_instance_template" {
  project = var.gcp_project_id
  name    = "${local.prefix}-template"

  machine_type = var.machine_type
  region = var.gcp_region
  tags = var.target_tags
  labels = var.instance_labels

  network_interface {
    network            = google_compute_network.dk_vpc.name
    subnetwork         = google_compute_subnetwork.dk_subnets.name
    subnetwork_project = var.gcp_project_id
    # access_config {
    #   nat_ip       = google_compute_address.dk_wordpress_addr.address
    #   network_tier = "STANDARD"
    # }
  }

  disk {
    auto_delete  = true
    boot         = true
    source_image = data.google_compute_image.dk_ubuntu.self_link
    type         = "PERSISTENT"
    disk_type    = var.disk_type
    disk_size_gb = var.disk_size_gb
  }

  service_account {
    email  = google_service_account.dk_wordpress_sa.email
    scopes = ["cloud-platform"]
  }

  metadata_startup_script = data.template_file.dk_user_data.rendered

  lifecycle {
    create_before_destroy = true
  }
}

# Managed Instance Group
resource "google_compute_health_check" "dk_health_checks" {
  name                = "${local.prefix}-health-check"
  check_interval_sec  = 60
  timeout_sec         = 30
  healthy_threshold   = 2
  unhealthy_threshold = 10

  http_health_check {
    request_path = "/wp-admin/images/wordpress-logo.svg"
  }
}

resource "google_compute_instance_group_manager" "dk_mig" {
  project            = var.gcp_project_id
  name               = "${local.prefix}-mig"
  description        = "DK Compute VM Managed Instance Group"
  wait_for_instances = true
  base_instance_name = local.prefix
  zone               = data.google_compute_zones.dk_zones.names[0]

  version {
    instance_template = google_compute_instance_template.dk_instance_template.self_link
    name              = "primary"
  }

  named_port {
    name = "http"
    port = var.wp_external_port
  }

  auto_healing_policies {
    health_check      = google_compute_health_check.dk_health_checks.id
    initial_delay_sec = 300
  }
}

resource "google_compute_autoscaler" "dk_autoscaler" {
  name   = "${local.prefix}-autoscaler"
  zone   = data.google_compute_zones.dk_zones.names[0]
  target = google_compute_instance_group_manager.dk_mig.id

  autoscaling_policy {
    max_replicas    = 2
    min_replicas    = 1
    cooldown_period = 60

    cpu_utilization {
      target = 0.5
    }
  }
}

# Forwarding rule and Load balancer
resource "google_compute_global_forwarding_rule" "dk_forwarding_rule" {
  name                  = "${local.prefix}-forwarding-rule"
  ip_protocol           = "TCP"
  load_balancing_scheme = "EXTERNAL"
  port_range            = "80"
  target                = google_compute_target_http_proxy.dk_http_proxy.id
  ip_address            = google_compute_global_address.dk_wordpress_addr.id
}

resource "google_compute_target_http_proxy" "dk_http_proxy" {
  name     = "${local.prefix}-http-proxy"
  url_map  = google_compute_url_map.dk_url_map.id
}

resource "google_compute_url_map" "dk_url_map" {
  name            = "${local.prefix}-lb-urlmap"
  default_service = google_compute_backend_service.dk_backend_service.id
}

resource "google_compute_backend_service" "dk_backend_service" {
  name                     = "${local.prefix}-backend-service"
  protocol                 = "HTTP"
  port_name                = "http"
  load_balancing_scheme    = "EXTERNAL"
  timeout_sec              = 10
  enable_cdn               = false
  health_checks            = [google_compute_health_check.dk_health_checks.id]
  backend {
    group           = google_compute_instance_group_manager.dk_mig.instance_group
    balancing_mode  = "UTILIZATION"
    capacity_scaler = 1.0
    max_utilization = 0.8
  }
}
