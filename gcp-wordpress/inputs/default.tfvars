# General
provision_name = "terraform-test"
provision_prefix = "dk"
gcp_project_id = "<project-id>"
gcp_region = "asia-southeast2"
gcp_routing_mode = "REGIONAL"
gcp_subnets = {
    subnet_name               = "dk-subnet-01"
    subnet_ip                 = "10.10.30.0/24"
    subnet_region             = "asia-southeast2"
    subnet_flow_logs_metadata = "EXCLUDE_ALL_METADATA"
}

# Instance Template
machine_type = "e2-small"
target_tags = ["egress-inet", "dk-allow-http", "allow-health-check"]
instance_labels = {
    env = "development"
}
disk_type = "pd-balanced"
disk_size_gb = 50

# Wordpress
wp_db_name = "wordpress"
wp_db_root_pass = "W0rdPr355"
wp_db_user = "wordpress"
wp_db_pass = "wordpre55"
wp_external_port = 80
