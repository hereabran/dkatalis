# DK Terraform Test

|Name | Nickname | Candidate |
|-----|----------|-----------|
| Muhammad Abdurrahman | `Abran` | SRE Amaan Project |


### Create Plan and Apply
```sh
cd gcp-wordpress
terraform plan -var-file inputs/default.tfvars 
```

#### Apply terraform plan
```sh
terraform apply -var-file inputs/default.tfvars 
```
![terraform apply](./gcp-wordpress/assets/1.png)
```sh
> Load Balancer IP: 34.111.59.205
```

### Open console and see Created GCP Managed Instance Group
- [x] Autoscaling `Min 1, Max 2`
- [x] Autohealing with Health Check
- [x] Startup Script/User Data from Instance Template
- [x] Not Publicly exposed, only Load Balancer have access to the VMs

![GCP Managed Instance Group](./gcp-wordpress/assets/4.png)
#### MIG in Properties
- Instance template
- Autoscaling
- Autohealing
- Networks
- Port mapping

![GCP Managed Instance Group Properties](./gcp-wordpress/assets/6.png)

#### Inside the VM instance
- [x] Docker container `UP` and `RUNNING`

![Load Balancer Detail](./gcp-wordpress/assets/2.png)
> Note: `temporary` open SSH session through Google IAP, after submission it completely `closed`

### Load Balancer Detail
- [x] Backend GCLB Service with Health Check

![Load Balancer Detail](./gcp-wordpress/assets/5.png)

### Wordpress Page (Installation Page)
>This is the evidence that Wordpress is `UP` and `RUNNING` on top of Docker Containers in Compute engine Managed by Google MIG and exposed by the Load Balancer `External IP`

![Load Balancer Detail](./gcp-wordpress/assets/3.png)

```sh
> Load Balancer IP: 34.111.59.205
```
#### Destroy terraform plan
```sh
terraform destroy -var-file inputs/default.tfvars 
```
![terraform destroy](./gcp-wordpress/assets/7.png)

**Thank you**
